# ubuntu-conda-pytorch

## Pulling  : 
```bash
docker pull mhadhbixissam/ubuntu-conda-pytorch:main
```
## Runing  : 
open brower on http://localhost:5001 after run the docker image : 
```bash
docker run --rm -it -p "5001:5001" mhadhbixissam/ubuntu-conda-pytorch:main
```
## Configure local machine : 
### Check nvidia drivers are installed  : 
```bash
nvidia-smi -a
```
if not working try  : 
```bash
sudo apt-get install linux-headers-$(uname -r)
distribution=$(. /etc/os-release;echo $ID$VERSION_ID | sed -e 's/\.//g')
wget https://developer.download.nvidia.com/compute/cuda/repos/$distribution/x86_64/cuda-keyring_1.0-1_all.deb
sudo dpkg -i cuda-keyring_1.0-1_all.deb
sudo apt-get update
sudo apt-get -y install cuda-drivers
```
> # Note : <span style="color:red"> Reboot your os after this installation </span>
### Install nvidia docker run-time on you machine  : 

```bash
curl -s -L https://nvidia.github.io/nvidia-container-runtime/gpgkey | sudo apt-key add -
distribution="ubuntu22.04"
curl -s -L https://nvidia.github.io/nvidia-container-runtime/$distribution/nvidia-container-runtime.list | sudo tee /etc/apt/sources.list.d/nvidia-container-runtime.list
sudo apt-get update
sudo apt-get install -y nvidia-container-toolkit
echo -e  '{\n    "default-runtime": "nvidia",\n    "runtimes": {\n        "nvidia": {\n            "path": "/usr/bin/nvidia-container-runtime",\n            "runtimeArgs": []\n        }\n    }\n}' > /etc/docker/daemon.json
sudo systemctl restart docker
```
Verify nvidia is setup for the docker  : 
```bash
sudo docker info | grep Runtime
```

