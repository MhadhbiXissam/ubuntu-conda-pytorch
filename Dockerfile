from  nvidia/cuda:11.8.0-runtime-ubuntu22.04
#************************************************

env DEBIAN_FRONTEND noninteractive
env HOME /root
env LANG en_US.UTF-8
env LANGUAGE en_US.UTF-8
env LC_ALL C.UTF-8
env color_prompt=yes
env PS1='\[\]\[\]\[\e[0;31m\]\u\[\e[0;31m\]@\[\e[34;1m\]\h\[\e[0m\]:\[\e[0;33m\]\w\[\e[0m\]\[\e[0;31m\] # \[\e[0m\]\[\]\[\]'
run apt update 
run apt install -y build-essential curl g++ git nano net-tools supervisor tar unzip wget
run mkdir /root/house

# #################################################
#              Terminal Theme                    #
# #################################################

run /usr/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh)" --prefix=/usr/local
run cp /usr/local/share/oh-my-bash/bashrc ~/.bashrc
run sed -i 's/OSH_THEME="[^"]*"/OSH_THEME="pure"/' ~/.bashrc
run sed -i 's/# OMB_PROMPT_SHOW_PYTHON_VENV=true  # enable/OMB_PROMPT_SHOW_PYTHON_VENV=true/' ~/.bashrc 


##################################################
#           install code-server                  #
##################################################
run curl -fsSL https://code-server.dev/install.sh | sh
run code-server --install-extension monokai.theme-monokai-pro-vscode
run echo -e '{\
    "workbench.colorTheme": "Monokai Classic",\
    "workbench.iconTheme": "Monokai Classic Icons" , \
    "window.customTitleBarVisibility": "auto",\
    "workbench.sideBar.location": "left",\
    "files.autoSave": "afterDelay" , \
    "window.commandCenter": false,\
    "workbench.layoutControl.enabled": false, \
    "workbench.activityBar.location": "top"\
}' > $HOME/.local/share/code-server/User/settings.json




# ##################################################
# #               install rust                     #
# ##################################################
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y
ENV PATH="$HOME/.cargo/bin:$PATH"


# ##################################################
# #               install nim 2.0                  #
# ##################################################
run cd /tmp/ && git clone https://github.com/nim-lang/Nim $HOME/.nimble && \
    cd $HOME/.nimble && git checkout version-2-0 && \
    bash build_all.sh && bin/nim c koch && ./koch boot -d:release && ./koch tools
ENV PATH="$HOME/.nimble/bin:$PATH"

##################################################
#           install conda                        #
##################################################
RUN wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh
RUN bash miniconda.sh -b -p /miniconda
RUN rm miniconda.sh
ENV PATH="/miniconda/bin:${PATH}"
RUN echo 'export PATH="/miniconda/bin:${PATH}"' >> /root/.bashrc
RUN conda config --set auto_activate_base false 
RUN conda create -n py3.11 python=3.11 
run conda init 
env CONDA_DEFAULT_ENV py3.11
run echo "conda activate $CONDA_DEFAULT_ENV"  >> /root/.bashrc
run conda  install -y pytorch torchvision torchaudio pytorch-cuda=11.8 -c pytorch -c nvidia
run conda run -n py3.11 pip install  django djangorestframework flask pillow twine


##################################################
#           install nodejs                       #
##################################################
run curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
run bash -c "source $HOME/.nvm/nvm.sh && nvm install 20 "
ENV PATH="$HOME/.nvm/versions/node/v20.13.1/bin/:${PATH}"
run npm install -g typescript  @angular/cli @types/react @types/react-dom vue@latest web-ext 


# ##################################################
# #               Add git config                   #
# ##################################################
env GIT_USER=example 
env GIT_EMIAL=example@gmail.com
env GIT_TOKEN=gplat
run echo "git config --global user.name $GIT_USER\
git config --global user.email $GIT_EMIAL\
git config --global credential.helper 'store --file $HOME/.git-credentials'\
if ! grep -q "https://$GIT_USER:$GIT_TOKEN@gitlab.com" "$HOME/.git-credentials"; then\
    git config --global user.name "$GIT_USER" && git config --global user.email "$GIT_EMAIL" &&  git config --global credential.helper 'store --file $HOME/.git-credentials'\
    echo "https://$GIT_USER:$GIT_TOKEN@gitlab.com" >> ~/.git-credentials\
fi" > /etc/profile.d/gitconfig.sh




cmd code-server --bind-addr 0.0.0.0:8001 --auth none /root/house

